const express = require('express');
const bodyParser = require('body-parser')
const app = express();
const fs = require("fs");
const axios = require('axios').default;

app.use(bodyParser.json());

app.get('/listUsers', function (req, res) {
   fs.readFile(__dirname + "/" + "users.json", 'utf8', function (err, data) {
      console.log(data);
      res.set({
         'Content-Type': 'application/json'
      })
      res.end(data);
   });
})

app.post('/*', function (req, res) {
   axios.post('https://uatws.thaivivat.co.th/crmsf/api' + req.url, req.body, {
      headers: {
         'Content-Type': 'application/json',
         'Accept': '*/*',
         'Authorization': req.headers.authorization
      }
   })
      .then(response => {
         res.set({
            'Content-Type': 'application/json'
         })
         .status(response.status)
         .json((typeof response.data === 'string') ? JSON.parse(response.data) : response.data);
      })
      .catch(err => {
         res.set({
            'Content-Type': 'application/json'
         })
         .sendStatus(500);
      });
})

app.get('*', function(req, res) {
   res.sendStatus(405);
})

const PORT = 8888;
const HOST = '0.0.0.0';

var server = app.listen(PORT, HOST, function () {
   var host = server.address().address
   var port = server.address().port
   console.log("Proxy app listening at http://%s:%s", host, port)
})